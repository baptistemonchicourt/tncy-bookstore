import java.util.ArrayList;

public class BookService {

    public BookService() {

    }

    public Book addBook(Book book) {
        return book;
    }

    public void removeBook(String bookId) {

    }

    public Book updateBook(String bookId, Book book) {
        return book;
    }

    public Book getBookById(String bookId) {
        return new Book(bookId, "", "");
    }

    public ArrayList<Book> findBookByTitle(String title) {
        return new ArrayList<Book>();
    }
}